import React, {useEffect, useState} from 'react';
import {FetchData} from "./fetchData";

const Example = () => {

  const [text,setText] = useState("????")
  const [secret,setSecret] = useState("")

  useEffect(() => {
    FetchData.getPipelineText()
      .then(data => {
        data instanceof Error ? setText(data.message) : setText( data.text)
      })
  }, []);

  const handleClick =  async () => {
    const result = await FetchData.getSecretText()
    result instanceof Error ? setSecret(result.message) : setSecret(result.text)
  }

  return (
    <>
      <h1 style={{color: "red"}}> Bienvenu sur le test </h1>
      <h2>Le text de l'api est :</h2>
      <p>{text}</p>
      <p>hey ho yo</p>
      <hr/>
      <h2>L'url de l'api est :</h2>
      {/* eslint-disable-next-line react/jsx-no-comment-textnodes */}
      <p>{window.location.protocol}//{window.location.hostname}/{process.env.REACT_APP_API_NAME || "????"}/pipeline</p>
      <button onClick={handleClick}>Get Secret</button>
      {secret && <p className="secret">{secret}</p> }
    </>
  );
};

export default Example;
