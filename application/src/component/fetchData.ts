import axios from "axios"

export class FetchData {
  static async getPipelineText () : Promise<{text: string} | Error> {
    try {
      const result = await axios.get(`${window.location.protocol}//${window.location.hostname}/${process.env.REACT_APP_API_NAME}/pipeline`)
      return result.data as {text: string}
    } catch (error) {
      return new Error("Erreur communication avec l'api")
    }
  }

  static async getSecretText () : Promise<{text: string} | Error>  {
    try {
      const result = await axios.get(`${window.location.protocol}//${window.location.hostname}/${process.env.REACT_APP_API_NAME}/secret`)
      return result.data as {text: string}
    } catch (error) {
      return new Error("Erreur communication avec l'api")
    }
  }


}
