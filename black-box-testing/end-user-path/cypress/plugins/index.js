import baseUrlPlugin from "./_base-url-plugin";
import reporterPlugin from "./_reporter-plugin";

export default async (on, config) => {
  if (!process.env.CI)
    baseUrlPlugin(config)

  reporterPlugin(on)

  return config
}
